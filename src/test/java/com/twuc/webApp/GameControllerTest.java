package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.HeaderResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    void should_return_201_status_when_its_be_called() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().is(201));
    }

    @Test
    void should_return_gameId_in_header() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().is(201))
                .andExpect(header().string("Location header", "/api/games/1"));

        mockMvc.perform(post("/api/games"))
                .andExpect(status().is(201))
                .andExpect(header().string("Location header", "/api/games/2"));
    }

    @Test
    void should_return_game_description_when_we_find_game_by_id() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().is(201))
                .andExpect(header().string("Location header", "/api/games/1"));

        mockMvc.perform(get("/api/games/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(mapper.writeValueAsString(GameList.gameList.get(0))));

        mockMvc.perform(get("/api/games/2"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_hint_message_with_patch_answer() throws Exception {
        mockMvc.perform(post("/api/games"));

        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"answer\": \"{1234}\" }"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("correct").value(false));
    }
}
package com.twuc.webApp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Random;

class Game {
    private int id;

    public int getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    private String answer;

    Game() {}

    Game(int id) {
        this.id = id;
        this.answer = String.valueOf(new Random().nextInt(9999) + 1);
    }

}

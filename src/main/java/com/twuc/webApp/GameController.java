package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RestController
public class GameController {
    int id = 1;

    @PostMapping("/api/games")
    ResponseEntity createGame() {
        Game game = new Game(id);
        GameList.gameList.add(game);
        id++;
        return ResponseEntity.status(201).header("Location header","/api/games/"+game.getId()).build();

    }

    @GetMapping("/api/games/{gameId}")
    ResponseEntity findGameWithId(@PathVariable int gameId) {
        for (Game game: GameList.gameList) {
            if (game.getId() == gameId)
                return ResponseEntity.status(200).contentType(MediaType.APPLICATION_JSON).body(game);
        }
        return ResponseEntity.status(404).build();
    }

    @PatchMapping("/api/games/{gameId}")
    ResponseEntity patchAnswerById(@PathVariable int gameId, @RequestBody Guess guess) {
        return ResponseEntity.ok().
                contentType(MediaType.APPLICATION_JSON)
                .body(new AnswerHint(GameList.gameList.get(gameId-1),guess));
    }

}

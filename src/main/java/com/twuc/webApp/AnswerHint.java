package com.twuc.webApp;

public class AnswerHint {
    private String hint;
    private Boolean correct;
    private Game game;
    private Guess guess;

    public AnswerHint() {
    }

    public AnswerHint(Game game, Guess guess) {
        this.game = game;
        this.guess = guess;
        checkAnswer();

    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }

    void checkAnswer() {
        int aCount = 0;
        int bCount = 0;
        for (int i = 0; i < 4; i++) {
            if (guess.getAnswer().charAt(i) == game.getAnswer().charAt(i)) {
                aCount++;
            }
        }
        hint += aCount + "A";
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (guess.getAnswer().charAt(i) == game.getAnswer().charAt(j) && i != j) {
                    bCount++;
                }
            }
        }
        hint += bCount + "B";

        if (hint == "4A0B")
           {
               correct = true;

           }
        else
            correct = false;

    }
}
